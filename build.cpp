#include <fstream>
#include <iostream>
#include <sstream>

#include "common.h"
#include "build.h"
#include "IndexandBool.h"
#include "rank.h"

using namespace std;

const string prefix = "text/";
const int tot = 10787;

vector<string> titles;
vector<string> docs;

int build(bool build_idx = true)
{
    for (int i = 0; i <= tot; ++i) {
        stringstream ss;
        ss << i;
        ifstream fin((prefix + ss.str() + ".txt").c_str());

        string title;
        getline(fin, title);
        
        titles.push_back(title);

        string doc;
        while(!fin.eof()) {
            string tmp;
            getline(fin, tmp);
            doc += tmp;
        }

        docs.push_back(doc);
    }
    build_rank();
    
    if (build_idx) {
        build_index(docs);
    }

	return 0;
}
