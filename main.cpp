#include <iostream>
#include <string>

#include "common.h"
#include "interp.h"

using namespace std;

string dictPath = "big.txt";

string format(const string in)
{
	string tmp = " ";
	for(unsigned int i = 0 ; i < in.length(); i++)
	{
		if(op(in[i]))
		{
			tmp.push_back(' ');
			tmp.push_back(in[i]);
			tmp.push_back(' ');
		}
		else
		{
			tmp.push_back(in[i]);
		}
	}
	tmp.push_back(' ');
	string t = "";
	for(unsigned int i = 1; i < tmp.length(); i++)
	{
		if((tmp[i] == tmp[i - 1]) && (tmp[i] == ' '))
		{
			continue;
		}
		t.push_back(tmp[i]);
	}
	return t;
}

int main(int argc, char* argv[])
{
	cout << "Loading dictionary..." << endl;
	corrector.load(dictPath);
	cout << "Dictionary loaded." << endl;
	if(argc > 1)
	{
		debug = true;
	}
	cout << "Welcome to Hamlet, which is a simple search engine for Shakespear's works." << endl;
	bool end = false;
	while (1) {
		string input = "";
		string t = "";
		while(t == "")
		{
			cout << ">>> ";
			getline(cin, input);
			if (cin.eof())
			{
				cout << endl;
				end = true;
				break;
			}
			t = format(input);
		}
		if(end)
		{
			break;
		}
		handler(t);
	}
	return 0;
}
