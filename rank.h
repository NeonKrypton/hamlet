//
//  rank.h
//  hamlet
//
//  Created by Zheng Yan on 6/18/14.
//  Copyright (c) 2014 Zheng Yan. All rights reserved.
//

#ifndef __hamlet__rank__
#define __hamlet__rank__

#include <iostream>

#include "common.h"

QueryResults rankResults(QueryResults, std::vector<std::string>);

void build_rank();

#endif /* defined(__hamlet__rank__) */
