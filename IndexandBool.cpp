#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <map>
#include <algorithm>
#include <cmath>

#include "IndexandBool.h"

#define D if(0)
#define E if(0)

vector<Word> indexes;
Word word[1000001];
map<string,int> vocabulary;

int cmp( const Word &a, const Word &b ){
    if( a.name < b.name )
       return 1;
    else
       return 0;
}

void ToJump(List *a)
{
	if(a==NULL) return;
	int l=0;
	List *temp;
	List *now=a;
	while(now!=NULL)
	{
		l++;
		now=now->next;
	}
	l=(int)(sqrt(l));
	int t=0;
	temp=a;
	now=a->next;
	while(now!=NULL)
	{
		t++;
		if(((t % l)==0)||(now->next==NULL))
		{
			temp->jump=now;
			temp=now;
		}
		now=now->next;
	}
}


void build_index(const vector<string> DOC)
{
D   cout<<"Begin"<<endl;

	// unsigned char c[100];
	int n=DOC.size();

    // index start from 0
	int tot = -1;

	for(int i=0;i<n;i++)
	{
		//DOC[i]=DOC[i]+" ";
D		cout<<"***"<<DOC[i]<<"***"<<endl;
		int j=0;
		int m = DOC[i].length();
		string t="";
		while(j<m)
		{
			if(DOC[i][j]==' ')
			{
				if(t!="")
				{
D				    cout<<t<<endl;
					List *temp=new(List);
					temp->doc=i;
					temp->next=NULL;
					temp->jump=NULL;
					if(vocabulary.count(t)==0)
					{
						tot++;
						vocabulary[t] = tot;
						word[tot].name=t;
						word[tot].head=word[tot].tail=temp;
						word[tot].len=1;
					}else
					{
						int k=vocabulary[t];
						if(word[k].tail->doc!=i)
						{
							word[k].tail->next=temp;
							word[k].tail=temp;
							word[k].len++;
						}
					}
				}
				t="";
			}else t=t+DOC[i][j];
			j++;
		}
	}
D   cout<<tot<<endl;
D   cout<<"************************\n\r***begin to write to the file***"<<endl;
	ofstream o1("index.txt");
	ofstream o2("index2.txt");
	sort(word+1,word+tot+1,cmp);
	indexes.clear();
D   cout<<"ready!\n\r";
	for(int i=1;i<=tot;i++)
	{
	    indexes.push_back(word[i]);
        //char blank=' ';
        o1<<word[i].name<<" "<<word[i].len;
D		o2<<word[i].name<<" "<<word[i].len;
		int l=0;
		List *now=word[i].head;
D       cout<<word[i].name<<" "<<word[i].len<<endl;
		while(now!=NULL)
		{
D           cout<<"test"<<endl;
D           cout<<" "<<now->doc<<endl;
			int t=now->doc-l;
			l=now->doc;
			o1<<" "<<t;
E			o2<<" "<<t;
			now=now->next;
		}
D       cout<<"finish!!"<<endl;
        o1<<endl;
E		o2<<endl;
	}
	o2.close();
	o1.close();//*/
}

List *indexSearch(const string a)
{
	int l=0;
	int r = indexes.size();
	while(l < r)
	{
		int m=(l+r)>>1;
		if(indexes[m].name<a)  l=m+1;else r=m;
	}
	if(indexes[l].name!=a) return NULL; else
    {
        
        List *temp=indexes[l].head;
        List *q,*p,*head;
        q=p=head=NULL;
        while(temp != NULL)
        {
            p=new(List);
            p->doc = temp->doc;
            if(q==NULL)
            {
                head=q=p;
            }
            else
            {
                q->next = p;
                q = p;
            }
            temp = temp->next;
        }
        return head;
    }
}

List *andSearch(List *a, List *b)
{
	List *head,*tail,*x,*y,*p;
	head=tail=NULL;
	x=a;
	y=b;
	while((x!=NULL)&&(y!=NULL))
	{
		//cout<<"x.doc"<<x->doc<<" y.doc"<<y->doc<<endl;
		if(x->doc==y->doc)
		{
			//cout<<"    "<<x->doc<<endl;
			p=new(List);
			p->doc=x->doc;
			p->next=p->jump=NULL;
			x=x->next;
			y=y->next;
			if(head==NULL)
			{
				head=tail=p;
			}else
			{
				tail->next=p;
				tail=p;
			}
		}else
		if(x->doc<y->doc)
		{
			if((x->jump!=NULL)&&(y->doc>=x->jump->doc)) x=x->jump;
			else x=x->next;
		}else
		{
			if((y->jump!=NULL)&&(x->doc>=y->jump->doc)) y=y->jump;
			else y=y->next;
		}
	}
	ToJump(head);
    if (a != NULL) {
        delete(a);
        cout << "a" << endl;
    }
    if (b != NULL) {
        delete(b);
        cout << "b" << endl;
    }
	return head;
}

List *orSearch(List *a, List *b)
{
	List *head,*tail,*x,*y,*p;
	head=tail=NULL;
	x=a;
	y=b;
	while((x!=NULL)||(y!=NULL))
	{
		p=new(List);
		p->next=p->jump=NULL;
		if((x==NULL)||((y!=NULL)&&(x->doc>y->doc)))
		{
			p->doc=y->doc;
			y=y->next;
		}else
		if((y==NULL)||(x->doc<y->doc))
		{
			p->doc=x->doc;
			x=x->next;
		}else
		if(x->doc==y->doc)
		{
			p->doc=x->doc;
			x=x->next;
			y=y->next;
		}
		if(head==NULL)
		{
			head=tail=p;
		}else
		{
			tail->next=p;
			tail=p;
		}
	}
	ToJump(head);
	delete(a);
	delete(b);
	return head;
}

List *notSearch(List *a,int d)
{
	List *head,*tail,*temp;
	head=tail=NULL;
	temp=a;
	List *p;
	for(int i=0; i < d;i++)
	{
		if((temp==NULL) || (i!=temp->doc))
		{
			p=new(List);
			p->doc=i;
			p->next=p->jump=NULL;
			if(head==NULL)
			{
				head=tail=p;
			}else
			{
				tail->next=p;
				tail=p;
			}
		}else temp=temp->next;
	}
	ToJump(head);
	delete(a);
	return head;
}

void load_index()
{
	ifstream i1("index.txt");
	indexes.clear();
	string a;
	while((i1>>a)&& !i1.eof())
	{
		int l;
		i1>>l;
//D       cout<<a<<" "<<l<<endl;
		Word now;
		now.name=a;
		now.len=l;
		now.tail=now.head=NULL;
		int n=0;

		for(int i=1;i<=l;i++)
		{
			int m;
			i1>>m;
			List *temp=new(List);
			n+=m;
			temp->doc=n;
			temp->next=temp->jump=NULL;
			if(i==1)
			{
				now.tail=now.head=temp;
			}else
			{
				now.tail->next=temp;
				now.tail=temp;
			}
		}
		ToJump(now.head);
		indexes.push_back(now);
	}
	i1.close();
}

void print_index()
{
    cout << "Begin to print index\n\r";
    int n = indexes.size();
    for(int i=0;i<n;i++)
    {
        cout << indexes[i].name << " " << indexes[i].len;
        List *p=indexes[i].head;
        while(p!=NULL)
        {
            cout<<" "<<p->doc;
            p=p->next;
        }
        cout<<endl;

    }
}
