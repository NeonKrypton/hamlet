//
//  porter.h
//  hamlet
//
//  Created by Zheng Yan on 6/17/14.
//  Copyright (c) 2014 Zheng Yan. All rights reserved.
//

#ifndef hamlet_porter_h
#define hamlet_porter_h

#include <string>

std::string stem(std::string input);

#endif
