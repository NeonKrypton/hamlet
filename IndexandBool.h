#ifndef _INDEXANDBOOL_H_
#define _INDEXANDBOOL_H_

#include <vector>
#include <string>

#include "common.h"

using namespace std;

void build_index(const vector<string>);
List *indexSearch(string);
List *andSearch(List*, List*);
List *orSearch(List*, List*);
List *notSearch(List*, int);
void load_index();
void print_index();

#endif
