#include "common.h"

#include <iostream>
#include <cmath>

using namespace std;

bool debug = false;

bool op(char c)
{
	if((c == '&') || (c == '|') 
	|| (c == '(') || (c == ')') || (c == '~'))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool op(std::string s)
{
	if(s.length() > 1)
	{
		return false;
	}
	return op(s[0]);
}

SpellCorrector corrector;

List::List():doc(0)
{
	next = jump = NULL;
}

List::~List()
{
	jump = NULL;
	if(next != NULL)
	{
		delete next;
	}
}

double cosine_distance(map<string, double> query, map<string, double> doc) {
    double dotProd = 0.0;
    double sumQ = 0.0, sumD = 0.0;
    
    for (auto it = doc.begin(); it != doc.end(); ++it) {
        sumD += it->second * it->second;
    }

    for (auto it = query.begin(); it != query.end(); ++it) {
        sumQ += it->second * it->second;
        auto t = doc.find(it->first);
        if (t != doc.end()) {
            dotProd += it->second * t->second;
        }
    }
    
    return dotProd / (sqrt(sumQ) * sqrt(sumD));
}

string head(const string str)
{
	unsigned long t = str.find(' ');
	return str.substr(0, t);
}

string tail(const string str)
{
	unsigned long t = str.find(' ');
	return str.substr(t + 1, str.length() - t - 1);
}
