# [DONE] Preprocess

* Lemmatization
* Just leaves words splited by space

# [TODO] Indexes

* Inverted indexes (Skip list)
* Positional indexes
* Permuterm indexes (B-tree)

# [TODO] Boolean query

* And
* Or
* Wildcard
* Positional

# [TODO] Command-line interface

* [DONE] Deal with input
* [TODO] Print result beautifully

# [TODO] Spell correction

* http://norvig.com/spell-correct.html

# [TODO] Query expansion

* Low priority

# [TODO] Ranking

* Low priority
