#include <iostream>
#include <utility>
#include <string>
#include <vector>
#include <stack>

#include "common.h"
#include "interp.h"
#include "build.h"
#include "search.h"
#include "IndexandBool.h"
#include "rank.h"

using namespace std;

#define _ERROR_BRACE 1

pair<int, ArgListType> parse(const string str)
{
	ArgListType ret;
	string lst = "( " + str + ") ";
	stack<string> s;
	int retState = 0;
	bool lastTokenOp = false;
	while(lst != "")
	{
		string token = head(lst);
		if((!op(token)) && (!lastTokenOp))
		{
			token = "&";
		}
		else
		{
			lst = tail(lst);
		}
		lastTokenOp = op(token);
		if(op(token))
		{
			if((token == "~") || (token == "("))
			{
				s.push(token);
			}
			else if(token == ")")
			{
				while((s.size() > 0) && (s.top() != "("))
				{
					ret.push_back(s.top());
					s.pop();
				}
				if(s.size() == 0)
				{
					return make_pair(_ERROR_BRACE, ret);
				}
				s.pop();
			}
			else
			{
				if((s.top() == "&") || (s.top() == "|"))
				{
					ret.push_back(s.top());
					s.pop();
				}
				s.push(token);
			}
		}
		else
		{
			ret.push_back(token);
			while(s.top() == "~")
			{
				ret.push_back(s.top());
				s.pop();
			}
		}
	}
    
    for (unsigned int i = 0; i < ret.size(); ++i) {
        ret[i] = stem(ret[i]);
    }

	return make_pair(retState, ret);
}

void printHelp()
{
	cout << "Available commands:\n\
    help:\n\
        Display this page.\n\
        Usage: help\n\
    build:\n\
        Build search engine index.\n\
        Usage: build\n\
    search:\n\
        Search document sets by given keywords and conditions.\n\
        Usage: search [-c] \"~\"?<keyword> ((\"&\"|\"|\") \"~\"?<keyword>)*\n\
        -c for using spell corrector.\n\
To quit Hamlet, use EOF(^Z in Windows or ^D in Unix).\n\n";
}

void printResults(QueryResults results)
{
    if (results.docIds.size() > 0 && results.docIds[0] == -1) {
        cout << "Need load index first." << endl;
    }
    else if (results.docIds.size() == 0) {
        cout << "Not resutls." << endl;
    }
    else {
        for (unsigned int i = 0; i < results.docIds.size(); ++i) {
            int docId = results.docIds[i];
            double score = results.scores[i];
            cout << docId << " ";
            cout << titles[docId] << " ";
            cout << score << endl;
        }
    }
}

void unknownCommand(const string cmd)
{
	cout << "Unknown command: \"" << cmd << "\"." << endl;
	cout << "Type \"help\" to get help." << endl;
}

void parseError(int errNo)
{
	string errInfo;
	switch(errNo)
	{
		case _ERROR_BRACE:
		{
			errInfo = "Brace error";
			break;
		}
		default:
		{
			errInfo = "Unknown error";
			break;
		}
	}
	cout << "Error: " << errInfo << endl;
}

void handler(const string input)
{
	string cmd = head(input);
	string args = tail(input);
	if(cmd == "help")
	{
		printHelp();
	}
	else if(cmd == "build")
	{
		if(build(true))
		{
			cout << "Index built failed." << endl;
		}
		else
		{
			cout << "Index built successfully." << endl;
		}
	}
	else if(cmd == "search")
	{
		if(head(args) == "-c")
		{
			string targs = tail(args);
			args = "";
			while(targs != "")
			{
				string thead = head(targs);
				if(!op(thead))
				{
					thead = corrector.correct(thead);
				}
				args = args + thead + " ";
				targs = tail(targs);
			}
			cout << "Searching for " << args << endl;
		}
		pair<int, ArgListType> t = parse(args);
		if(t.first)
		{
			parseError(t.first);
		}
		else
		{
			QueryResults results = search(t.second);
            results = rankResults(results, t.second);
			printResults(results);
		}
	}
    else if (cmd == "load")
    {
        cout << "Loading Index" << endl;
        load_index();
        build(false);
    }
	else
	{
		unknownCommand(cmd);
	}
}
