#ifndef _SEARCH_H_
#define _SEARCH_H_

#include <string>

#include "common.h"

QueryResults search(const ArgListType input);

QueryResults _search(const ArgListType input);

List *andResults(List *results1, List *results2);

List *orResults(List *results1, List *results2);

List *notResults(List *results1);

#endif
