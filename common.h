#ifndef _COMMON_H_
#define _COMMON_H_

#include <vector>
#include <string>
#include <map>

#include "SpellCorrector.h"

double cosine_distance(std::map<std::string, double> query, std::map<std::string, double> doc);

std::string head(const std::string str);

std::string tail(const std::string str);

struct QueryResults
{
    std::vector<int> docIds;
    std::vector<double> scores;
};

typedef std::vector<std::string> ArgListType;

extern bool debug;

bool op(char c);
bool op(std::string s);

class List
{
public:
    int doc;
    List *next,*jump;
    List();
    ~List();
};

struct Word
{
    std::string name;
    List *head,*tail;
    int len;
};

extern std::vector<Word> indexes;

extern SpellCorrector corrector;

extern std::vector<std::string> titles;
extern std::vector<std::string> docs;

extern const int tot;

extern std::map<std::string,int> vocabulary;

#endif
