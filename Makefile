CC = g++
CPPFLAGS = -Wall -O3 -DDEBUG -std=c++11
HEADERS = common.h interp.h build.h search.h IndexandBool.h SpellCorrector.h porter.h rank.h
SOURCES = main.cpp interp.cpp build.cpp search.cpp common.cpp IndexandBool.cpp SpellCorrector.cpp porter.cpp rank.cpp
OBJECTS = main.o interp.o build.o search.o common.o IndexandBool.o SpellCorrector.o porter.o rank.o

hamlet: $(HEADERS) $(SOURCES) $(OBJECTS)
	$(CC) $(CPPFLAGS) $(OBJECTS) -o hamlet -DDEBUG 

main.o: common.h interp.h

interp.o: common.h interp.h build.h search.h porter.h

build.o: common.h build.h

search.o: common.h search.h

common.o: common.h

IndexandBool.o: common.h IndexandBool.h

SpellCorrector.o: SpellCorrector.h

porter.o: porter.h

rank.o: common.h rank.h

clean: 
	rm $(OBJECTS) hamlet hamlet.exe
