import os
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer
from nltk.stem.porter import *


def main():
    tokenizer = RegexpTokenizer(r'\w+')
    wnl = WordNetLemmatizer()
    stemmer = PorterStemmer()

    count = 0
    for filename in sorted(os.listdir('Reuters'), key=lambda x: int(x.split('.')[0])):
        if not filename.endswith('html'):
            continue
        print filename
        f = open('Reuters/{}'.format(filename))
        o = open('text/{}.txt'.format(count), 'w')
        head = True
        for line in f:
            if head:
                o.write(line)
                head = False
            else:
                tokens = map(lambda x: stemmer.stem(x.lower()), tokenizer.tokenize(line))
                if tokens:
                    o.write(' '.join(tokens) + ' ')
        count += 1


if __name__ == '__main__':
    main()
