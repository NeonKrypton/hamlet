#include <iostream>
#include <cassert>

#include "search.h"
#include "IndexandBool.h"

using namespace std;

QueryResults search(const ArgListType input)
{
#ifdef DEBUG
    for (unsigned int i = 0; i < input.size(); ++i) {
        cout << input[i] << " ";
    }
    cout << endl;
#endif
    QueryResults results;
    if (indexes.size() == 0) {
        results.docIds.push_back(-1);
    }
    else
    {
        results = _search(input);
    }
    return results;
}

QueryResults _search(ArgListType input) {
    List *result = NULL;

    unsigned int idx = 0;
    vector<List*> lists;
    while ((!input.empty())&&(idx < input.size())) {
        int length = lists.size();
        if (input[idx] == "&") {
            assert(lists.size() >= 2);
            List *res = andResults(lists[length - 1], lists[length - 2]);
            lists.pop_back();
            lists.pop_back();
            lists.push_back(res);
            input.erase(input.begin() + idx);
            input.erase(input.begin() + idx - 1);
            idx -= 2;
        }
        else if (input[idx] == "|") {
            assert(lists.size() >= 2);
            List *res = orResults(lists[length - 1], lists[length - 2]);
            lists.pop_back();
            lists.pop_back();
            lists.push_back(res);
            input.erase(input.begin() + idx);
            input.erase(input.begin() + idx - 1);
            idx -= 2;
        }
        else if (input[idx] == "~") {
            assert(lists.size() >= 1);
            List *res = notResults(lists[length - 1]);
            lists.pop_back();
            lists.push_back(res);
            input.erase(input.begin() + idx);
            idx -= 1;
        }
        else {
            result = indexSearch(input[idx]);
            lists.push_back(result);
        }
        idx += 1;
        cout << idx << endl;
    }

    QueryResults ret;

    if (lists.size() == 1 && lists[0] != NULL) {
        List *cur = lists[0];
        do {
            ret.docIds.push_back(cur->doc);
            cur = cur->next;
        } while(cur);
    }

    return ret;
}

List *andResults(List *results1, List *results2)
{
    List *results = andSearch(results1, results2);
    return results;
}

List *orResults(List *results1, List *results2)
{
    List *results = orSearch(results1, results2);
    return results;
}

List *notResults(List *results1)
{
    List *results = notSearch(results1, tot);
    return results;
}
