//
//  rank.cpp
//  hamlet
//
//  Created by Zheng Yan on 6/18/14.
//  Copyright (c) 2014 Zheng Yan. All rights reserved.
//

#include <string>
#include <complex>
#include <algorithm>

#include "rank.h"

using namespace std;

vector<map<string, double>> tf;
map<string, double> df;
map<string, double> idf;
vector<map<string, double>> tfidf;

void build_rank() {
    for (unsigned int i = 0; i < docs.size(); ++i) {
        map<string, double> m;
        vector<string> v;
        string doc = docs[i];
        while(doc != "")
        {
            string d = head(doc);
            
            auto t = df.find(d);
            if (t == df.end()) {
                df[d] = 1;
            }
            else {
                df[d] += 1;
            }
            
            auto it = m.find(d);
            if (it == m.end()) {
                m[d] = 1;
            }
            else {
                m[d] += 1;
            }
            v.push_back(d);
            doc = tail(doc);
        }
        tf.push_back(m);
    }

    for (auto it = df.begin(); it != df.end(); ++it) {
        idf[it->first] = log10(double(tot) / it->second);
    }

    for (auto i = tf.begin(); i != tf.end(); ++i) {
        map<string, double> m;
        for (auto j = (*i).begin(); j != (*i).end(); ++j) {
            m[j->first] = (1.0 + log10(j->second)) * idf[j->first];
        }
        tfidf.push_back(m);
    }
}

QueryResults rankResults(QueryResults r, vector<string> q) {
    map<string, double> df;
    for (unsigned int i = 0; i < q.size(); ++i) {
        if (q[i] != "&" && q[i] != "|" && q[i] != "~") {
            auto t = df.find(q[i]);
            if (t == df.end()) {
                df[q[i]] = 1;
            }
            else {
                df[q[i]] += 1;
            }
        }
    }
    map<string, double> queryVector;
    for (auto j = df.begin(); j != df.end(); ++j) {
        queryVector[j->first] = (1.0 + log10(j->second)) * idf[j->first];
    }
    
    map<int, double> scores;
    for (unsigned int i = 0; i < r.docIds.size(); ++i) {
        scores[r.docIds[i]] = cosine_distance(queryVector, tfidf[r.docIds[i]]);
    }

    vector<pair<int, double>> v;
    for(auto i = scores.begin(); i != scores.end(); ++i) {
        v.push_back(make_pair(i->first, i->second));
    }
    sort(v.begin(),v.end(),[](const pair<int,double>&a,const pair<int,double>&b){return a.second>b.second;});

    QueryResults ret;
    for (auto it = v.begin(); it != v.end(); ++it) {
        ret.docIds.push_back(it->first);
        ret.scores.push_back(it->second);
    }

    return ret;
}
