#ifndef _INTERP_H_
#define _INTERP_H_

#include <string>

#include "common.h"
#include "porter.h"

void handler(const std::string input);

#endif
